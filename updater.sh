#!/bin/bash

VER_FILE="ver"
VER=0

if [ -f ${VER_FILE} ]; then
    VER=`cat ${VER_FILE}`
fi

if (( VER < 3 )); then

cat <<EOF > /usr/local/ip-free/ipfree.sh
#!/bin/bash


git clone https://gitlab.com/demonand/ip-free.git

if [ -f ./ip-free/updater.sh ]; then
    bash ./ip-free/updater.sh
fi

FILE_D1="./ip-free/domain"
FILE_D2="domain"
FILE_I1="./ip-free/ip"
FILE_I2="ip"
cp ip-free/run.sh .
chmod 777 run.sh

IPDOMAIN=""

domainproc()
{
    local DOMAIN=\`dig +short \$1 | grep -E -o "[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+"\`
    if [[ \$DOMAIN =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\$ ]]; then
        IPDOMAIN="\${IPDOMAIN}\\n#\${1}\\n\${DOMAIN}/32"
    fi
}
readfile()
{
while read line; do
# reading each line
\$2 "\$line"
done < \$1
}


readfile "\${FILE_D1}" "domainproc"
echo -e \${IPDOMAIN} >> \${FILE_I1}
sed -i '/^\$/d' \${FILE_I1}



if cmp --silent -- "\$FILE_D1" "\$FILE_D2" && cmp --silent -- "\$FILE_I1" "\$FILE_I2" ; then
  echo "files contents are identical"
else
    if command -v fail2ban-regex &>/dev/null; then
        systemctl stop fail2ban
    fi

    cp ip-free/domain .
    cp ip-free/ip .
    ./run.sh
    /usr/bin/systemctl restart iptables
    if command -v fail2ban-regex &>/dev/null; then
        systemctl start fail2ban
    fi
	if command -v docker &>/dev/null; then
    	if [ ! \$(docker ps | wc -l) == "1" ]; then
        	/usr/bin/systemctl restart docker
    	fi
	fi
fi

rm -rf ip-free
EOF

chmod 777 /usr/local/ip-free/ipfree.sh

echo "3" > ${VER_FILE}
fi



