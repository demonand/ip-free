#!/bin/bash

if [ -f ./ip-free/updater.sh ]; then
    bash ./ip-free/updater.sh
fi


IPTAB="#start-ip-free\n"
IPTABLES="/etc/sysconfig/iptables"
IPTABLES_NEW="iptables_new"
IPTABLES_NEW_CLEAN="iptables_new.clean"


readfile()
{
    while read line; do
        $2 "$line"
    done < $1
}
ipproc()
{
if [[ "$1" =~ ^#.* ]]; then
    IPTAB="${IPTAB}\n${1}"
else
    IPTAB="${IPTAB}\n-A INPUT -s $1 -j ACCEPT"
fi
}

domainproc()
{
    local DOMAIN=`dig +short $1dig +short $1 | grep -E -o "[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+"`
	if [[ $DOMAIN =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
		IPTAB="${IPTAB}\n#${1}\n-A INPUT -s ${DOMAIN}/32 -j ACCEPT"
	fi
}

INS=0
FIL=0

insertip()
{
if [[ "$1" =~ ^\*filter.* ]]; then
    FIL=1
fi
if [[ "$FIL" == 1 ]]; then
    if [[ "$1" =~ ^\:OUTPUT.*ACCEPT.* ]]; then
        echo $1 >> $IPTABLES_NEW
        echo "${IP}" >> $IPTABLES_NEW
    fi
fi

if [[ "$INS" == 0 ]] && [[ ! "$1" =~ ^\:OUTPUT.*ACCEPT.* ]]; then
    echo $1 >> $IPTABLES_NEW
fi

if [[ "$1" =~ ^COMMIT.* ]]; then
    FIL=0
fi

}

replaceip()
{
if [[ "$1" =~ ^#start-ip-free.* ]]; then
    INS=1
    echo "${IP}" >> $IPTABLES_NEW
fi
if [[ "$INS" == 0 ]]; then
    echo $1 >> $IPTABLES_NEW
fi

if [[ "$1" =~ ^#end-ip-free.* ]]; then
    INS=0
fi
}

readfile "ip" "ipproc"
#readfile "domain" "domainproc"
IPTAB="
    ${IPTAB}\n\n#end-ip-free"

echo -e $IPTAB > tmp

IP=$(cat tmp)
echo -n "" > $IPTABLES_NEW

if grep -Fxq "#start-ip-free" $IPTABLES
then
    readfile $IPTABLES "replaceip"
else
    readfile $IPTABLES "insertip"
fi
DATE=`date +'%m-%d-%Y'`
cp -f $IPTABLES iptables_$DATE.bak
awk '/^-A/ && !seen[$0]++ || !/^-A/' $IPTABLES_NEW > $IPTABLES_NEW_CLEAN
cp -f $IPTABLES_NEW_CLEAN $IPTABLES


rm -f $IPTABLES_NEW
rm -f $IPTABLES_NEW_CLEAN
rm -f tmp
